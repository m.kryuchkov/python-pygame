import random
import pygame

SIZE = 64 # Размер картинки препятствия (квадрат).
SPEED = 3 # Начальная скорость движения препятствия.

class Trash(pygame.sprite.Sprite):
    """Препятствие (мусор)

    Аттрибуты:
        group   Группа, объединяющая все препятствия
        counter Счётчик количества препятствий, покинувших экран
    """
    group = pygame.sprite.Group()
    counter = 0
    
    def __init__(self, screen_width, ground):
        """Создание препятсвия

        Аргументы:
            screen_width (целое число): ширина экрана
            ground (целое число): координата уровня земли
        
        Аттрибуты:
            image (изображение): картинка
            screen_width (целое число): ширина экрана
            rect (прямоугольник): прямоугольник, соответствующий картинке
            speed (целое число): скорость движения в пикселях
        """
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(
            'image/dump.png').convert_alpha()
        self.image = pygame.transform.smoothscale(self.image, (SIZE, SIZE))
        self.screen_width = screen_width
        self.rect = self.image.get_rect(center = (
            self.get_new_x(),
            ground - SIZE / 2
        ))
        self.speed = SPEED
        Trash.group.add(self)
    
    def update(self):
        """Правила изменения координат и свойств препятствия
        """
        # Движение налево
        self.rect.x -= self.speed

        # Препятствие ушло за пределы экрана слева
        if (self.rect.x <= -SIZE):
            Trash.counter += 1
            self.rect.x = self.get_new_x()
    
    def get_new_x(self):
        """Получение новой координаты X препятствия.

        Возвращает:
            целое число: случайная координата X за пределами окна справа
        """
        return self.screen_width + SIZE \
                + random.randint(0, self.screen_width)
