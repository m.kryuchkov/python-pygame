import pygame
from сat import Cat
from trash import Trash

FPS = 60 # частота перерисовки окна (кадров в секунду)
WIDTH = 960 # ширина окна
HEIGHT = 480 # высота окна
BACKGROUND = (182, 193, 255) # цвет фона в формате RGB (Red: 182; Green: 193: Blue:255)

def main():
    """Главная функция, запуск и работа игры.
    """
    # Настройка окна игры (дисплея)
    pygame.init()
    display = pygame.display.set_mode((WIDTH, HEIGHT))
    display.fill(BACKGROUND)
    
    # Создание игровых часов
    clock = pygame.time.Clock()

    # Подготовка шрифта и надписи "GAME OVER"
    font = pygame.font.Font(None, 36)
    game_over = font.render('GAME OVER', True, (180, 0, 0))
    game_over_rect = game_over.get_rect(center=(WIDTH / 2, HEIGHT / 2))

    # Создание персонажа
    cat = Cat(128, HEIGHT)

    # Создание препятствий
    for x in range(3):
        Trash(WIDTH, HEIGHT)
    
    is_playing = True # Происходит ли обновление игровых объектов

    # Игровой цикл
    while True:
        # События в pygame
        for event in pygame.event.get():
            # Выход из игры и закрытие окна
            if (event.type == pygame.QUIT):
                pygame.quit()
                return
            # Нажатия клавиш
            if (event.type == pygame.KEYDOWN):
                # Клавиша со стрелкой вверх
                if (event.key == pygame.K_UP):
                    cat.jump()
                # Клавиша со стрелкой вниз
                if (event.key == pygame.K_DOWN):
                    cat.fall()

        # Обновление координат и свойств игровых объектов
        if (is_playing):
            cat.update() # Обновление персонажа
            Trash.group.update() # Обновление всех препятствий
        
        # Перерисовка окна
        display.fill(BACKGROUND)
        Trash.group.draw(display)
        display.blit(cat.image, cat.rect)
        score = font.render(f"Score: {Trash.counter}", True, (10, 100, 50))
        display.blit(score, (10, 10))
        if (not is_playing):
            display.blit(game_over, game_over_rect)
        
        # Проверка столкновений (коллизий)
        #   Если какое-то из препятствий столкнулось с персонажем,
        #   то обновление свойств надо прекратить.
        if (pygame.sprite.spritecollideany(cat, Trash.group,
                pygame.sprite.collide_rect_ratio(0.5))):
            is_playing = False

        # Обновление содержимого окна
        pygame.display.update()

        # Обращение к игровым часам для задержки между итерациями игрового цикла
        clock.tick(FPS)

# Если данный модуль запустили, вызываем функцию main
if __name__ == "__main__":
    main()