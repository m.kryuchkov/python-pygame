from enum import Enum
import pygame

SIZE = 128 # Размер картинки персонажа (квадрат).
JUMP_HEIGHT = 80.0 # Высота/сила прыжка персонажа.
JUMP_K = 0.66 # Коэффициент сопротивления прыжку (гравитация и прочее).
JUMP_MIN_PWR = 0.00001 # Пороговое значения силы прыжка. Значения меньше считаем нулём.

class Direction(Enum):
    """Перечень направлений движения персонажа (для класса Cat).
    """
    NONE = 0
    UP = 1
    DOWN = 2

class Cat(pygame.sprite.Sprite):
    """Персонаж (котейка)
    """
    def __init__(self, x, y):
        """Создание персонажа

        Аргументы:
            x (целое число): начальная кордината X
            y (целое число): начальная кордината Y
        
        Аттрибуты:
            image (изображение): картинка
            rect (прямоугольник): прямоугольник, соответствующий картинке
            jump_power (дробное число): начальное значение силы прыжка
            direction (Direction): начальное направление движения персонажа
            ground (целое число): координата Y уровня земли
        """
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load(
            'image/cat.png').convert_alpha()
        self.image = pygame.transform \
            .smoothscale(self.image, (SIZE, SIZE))
        self.rect = self.image.get_rect(
            center = (x, y - SIZE / 2)
        )
        self.jump_power = 0.0
        self.direction = Direction.NONE
        self.ground = self.rect.y # считаем уровнем земли начальную координату
    
    def update(self):
        """Правила изменения координат и свойств персонажа
        """
        # Движение вверх
        if (self.direction == Direction.UP):
            self.rect.y -= self.jump_power
            self.jump_power = self.jump_power * JUMP_K
            # Верхнняя точка => движемся вниз
            if (self.jump_power < JUMP_MIN_PWR):
                self.jump_power = JUMP_MIN_PWR
                self.direction = Direction.DOWN
        # Движение вниз
        elif (self.direction == Direction.DOWN):
            self.rect.y += self.jump_power
            self.jump_power = self.jump_power / JUMP_K

        # Удар о землю => прекращение движения
        if (self.rect.y > self.ground):
            self.rect.y = self.ground
            self.jump_power = 0
            self.direction = Direction.NONE

    def jump(self):
        """Задаёт импульс для прыжка
        """
        # Отталкиваемся только от земли
        if (self.rect.y >= self.ground):    
            self.direction = Direction.UP
            self.jump_power = JUMP_HEIGHT
    
    def fall(self):
        """Задаёт импулья для резкого падения в прыжке
        """
        # Падаем только в воздухе
        if (self.rect.y < self.ground):
            self.direction = Direction.DOWN
            self.jump_power = JUMP_HEIGHT * 2
