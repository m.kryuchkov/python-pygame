# Учебный пример игры, созданной на PyGame

Суть игры - перепрыгивать препятствия и продержаться без столкновений как можно дольше.
Управление осуществляется при помощи стрелок на клавиатуре.
Для выхода из игры следует закрыть окно игры.

![Снимок экрана игры](./screenshots/sample.png)
![Снимок экрана игры](./screenshots/gameover.png)

## Как запустить

Для запуска необходимо установить:

- Python ([ссылка](https://www.python.org/downloads/));
- Pygame (выполнить команду `py -m pip install -U pygame --user` в терминале);
- Редактор кода, например, VS Code ([ссылка](https://code.visualstudio.com/download)) и расширение для поддержки Python ([ссылка](https://marketplace.visualstudio.com/items?itemName=ms-python.python)).

Для запуска игры из редактора кода следует запустить файл `main.py` из папки с файлами игры.

Для заупска игры без редактора следует запустить команду `py main.py` из папки с файлами игры.

## Вопросы и ответы

Можно связаться через почту [kidslearntocode@outlook.com](mailto:kidslearntocode@outlook.com).
